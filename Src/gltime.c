#include "stm32f4xx.h"
#include "gltime.h"

#define ticks (SystemCoreClock/1000)

volatile uint32_t tic_toc;

uint8_t TimeoutStart(void)
{
	if(SysTick->CTRL & SysTick_CTRL_ENABLE_Msk)
		return 1;
  else 
	{
		tic_toc=0x0U;
		SysTick_Config(ticks);
		return 0;
	}
}

void TimeoutStop(void)
{
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}

uint32_t GetTickTimeout(void)
{
	return tic_toc;
}

void Delay_ms(uint32_t time_ms)
{
  tic_toc=0x0U;
  SysTick_Config(ticks);
	while(tic_toc<time_ms){}
  SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}

void Delay_us(uint32_t time_us)
{
    uint32_t LD = SystemCoreClock / 1000000 * time_us;
  if (LD > 16777215 || time_us == 0)
		return;
  SysTick->CTRL &= ~SysTick_CTRL_TICKINT_Msk;
	SysTick->CTRL |= SysTick_CTRL_CLKSOURCE_Msk;
  SysTick->LOAD = LD;
	SysTick->VAL = 0;
	SysTick->CTRL |= SysTick_CTRL_ENABLE_Msk;
	while (!(SysTick->CTRL & SysTick_CTRL_COUNTFLAG_Msk));
	SysTick->CTRL &= ~SysTick_CTRL_COUNTFLAG_Msk;
	SysTick->CTRL &= ~SysTick_CTRL_ENABLE_Msk;
}
void SysTick_Handler(void)  //прерывание от системного таймера, нужно для реализации задержек
{
  tic_toc++;
}
