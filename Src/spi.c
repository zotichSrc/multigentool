#include "spi.h"
#include "gltime.h"

void WriteByteSPI(SPI_TypeDef *SPIx, uint8_t byte)
{
	uint16_t count = 0;
	while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
	count = 0;
	SPIx->DR = byte;
	while(!(SPIx->SR & SPI_SR_RXNE) && count < 60000) count++;
	uint8_t tmp = SPIx->DR;
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
}

uint8_t ReadByteSPI(SPI_TypeDef *SPIx)
{
	uint16_t count = 0;
	while(!(SPIx->SR & SPI_SR_TXE));
	SPIx->DR = 0x00;
	while(!(SPIx->SR & SPI_SR_RXNE) && count < 60000) count++;
	uint8_t tmp = SPIx->DR;
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
	return tmp;
}

uint8_t WriteReadByteSPI(SPI_TypeDef *SPIx, uint8_t byte)
{
	uint16_t count = 0;
	while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
	count = 0;
	SPIx->DR = byte;
	while(!(SPIx->SR & SPI_SR_RXNE) && count < 60000) count++;
	uint8_t tmp = SPIx->DR;
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
	return tmp;
}

void WriteDataSPI(SPI_TypeDef *SPIx, uint8_t* data, uint32_t len, uint8_t MSBFirst)
{
	uint16_t count = 0;
	uint16_t tmp = 0;
	if(MSBFirst)
	{
		uint32_t TxCount = len;
		while(TxCount > 0)
		{
			count = 0;
			while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
			count = 0;
			SPIx->DR = data[TxCount - 1];
			TxCount--;
			while(!(SPIx->SR & SPI_SR_RXNE)  && count < 60000) count++;
			tmp = (uint16_t)SPIx->DR;
		}
	}
	else
	{
		uint32_t TxCount = 0;
		while(TxCount < len)
		{
			count = 0;
			while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
			count = 0;
			SPIx->DR = data[TxCount];
			TxCount++;
			while(!(SPIx->SR & SPI_SR_RXNE)  && count < 60000) count++;
			tmp = (uint16_t)SPIx->DR;
		}
	}
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
}

void ReadDataSPI(SPI_TypeDef *SPIx, uint8_t *datain, uint32_t len, uint8_t MSBFirst)
{
	uint16_t count = 0;
	uint32_t RxCount = len;
	if(MSBFirst)
	{
		while(RxCount > 0)
		{
			count = 0;
			while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
			count = 0;
			SPIx->DR = 0x00;
			while(!(SPIx->SR & SPI_SR_RXNE) && count < 60000) count++;
			datain[RxCount-1] = (uint8_t)SPIx->DR;
			RxCount--;
		}
	}
	else
	{
		RxCount = 0;
		while(RxCount < len)
		{
			count = 0;
			while(!(SPIx->SR & SPI_SR_TXE) && count < 60000) count++;
			SPIx->DR = 0x00;
			count = 0;
			while(!(SPIx->SR & SPI_SR_RXNE) && count < 60000) count++;
			datain[RxCount] = (uint8_t)SPIx->DR;
			RxCount++;
		}
	}
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
}

void WriteReadDataSPI(SPI_TypeDef *SPIx, uint8_t* dataout, uint8_t *datain, uint32_t len, uint8_t MSBFirst)
{
	uint32_t TxCount = len;
	uint32_t RxCount = len;
	uint16_t count = 0;
	if(MSBFirst)
	{
		while(TxCount > len)
		{
			while(!(SPIx->SR & SPI_SR_TXE)&& count < 60000) count++;
			count = 0;
			SPIx->DR = dataout[TxCount-1];
			TxCount--;
			while(!(SPIx->SR & SPI_SR_RXNE)  && count < 60000) count++;
			datain[RxCount-1] = (uint8_t)SPIx->DR;
			RxCount--;
			count = 0;
		}
	}
	else
	{
		TxCount = 0;
		RxCount = 0;
		while(TxCount < len)
		{
			while(!(SPIx->SR & SPI_SR_TXE));
			SPIx->DR = dataout[TxCount];
			TxCount++;
			while(!(SPIx->SR & SPI_SR_RXNE)  && count < 60000) count++;
			datain[RxCount] = SPIx->DR;
			RxCount++;
			count = 0;
		}
	}
	count = 0;
	while((SPIx->SR & SPI_SR_BSY) && count < 60000) count++;
}

void WriteReadDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t* dataout, uint8_t *datain, uint8_t len)
{
	SPIx->CR2 |= SPI_CR2_RXDMAEN;
	
	DMA2_Stream3->CR &= ~DMA_SxCR_EN;
	DMA2_Stream3->NDTR = len;
	DMA2_Stream3->PAR = (uint32_t)(&SPI1->DR);
	DMA2_Stream3->M0AR = (uint32_t)(dataout);
	DMA2_Stream3->CR |= DMA_SxCR_MINC;
	DMA2_Stream3->CR &= ~DMA_SxCR_PINC;
	DMA2_Stream3->CR |= DMA_SxCR_EN;
	
	DMA2_Stream0->CR &= ~DMA_SxCR_EN;
	DMA2_Stream0->NDTR = len;
	DMA2_Stream0->PAR = (uint32_t)(&SPIx->DR);
	DMA2_Stream0->M0AR = (uint32_t)(datain);
	DMA2_Stream0->CR &= ~DMA_SxCR_MINC;
	DMA2_Stream0->CR &= ~DMA_SxCR_PINC;
	DMA2_Stream0->CR |= DMA_SxCR_EN;
	//Не доделано. Настроить прерывание по приему от DMA
}
void ReadDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t *datain, uint8_t len)
{
	uint8_t dummy = 0x00;
	SPIx->CR2 |= SPI_CR2_RXDMAEN;
	
	DMA2_Stream3->CR &= ~DMA_SxCR_EN;
	DMA2_Stream3->NDTR = len;
	DMA2_Stream3->PAR = (uint32_t)(&SPI1->DR);
	DMA2_Stream3->M0AR = (uint32_t)(&dummy);
	DMA2_Stream3->CR |= DMA_SxCR_MINC;
	DMA2_Stream3->CR &= ~DMA_SxCR_PINC;
	DMA2_Stream3->CR |= DMA_SxCR_EN;
	
	DMA2_Stream0->CR &= ~DMA_SxCR_EN;
	DMA2_Stream0->NDTR = len;
	DMA2_Stream0->PAR = (uint32_t)(&SPIx->DR);
	DMA2_Stream0->M0AR = (uint32_t)(datain);
	DMA2_Stream0->CR &= ~DMA_SxCR_MINC;
	DMA2_Stream0->CR &= ~DMA_SxCR_PINC;
	DMA2_Stream0->CR |= DMA_SxCR_EN;
}
void WriteDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t* data, uint8_t len)
{}
