#include "stm32f4xx.h"
#include "LCD.h"
#include "gltime.h"


#ifndef LCD_I2C

#define E_GPIO	(GPIOA)
#define E_ODR		(GPIO_ODR_ODR12)
#define RS_GPIO	(GPIOB)
#define RS_ODR	(GPIO_ODR_ODR15)
#define RW_GPIO	(GPIOB)
#define RW_ODR	(GPIO_ODR_ODR14)

#define LCD_GPIO	(GPIOA)
#define LCD_CONF	(LCD_GPIO->CRH)
#define LCD_OUT		(LCD_GPIO->ODR)
#define LCD_IN		(LCD_GPIO->IDR)

#define LCD_GPIO_MODE			((uint32_t)0X00003333)
#define LCD_GPIO_MODE_OUT	(LCD_GPIO_MODE & ((uint32_t)0x1111))
#define LCD_GPIO_MODE_IN	(LCD_GPIO_MODE & ((uint32_t)0x0000))
#define LCD_GPIO_CNF			((uint32_t)0X0000CCCC)
#define LCD_GPIO_CNF_OUT	(LCD_GPIO_CNF & ((uint32_t)0X0000))
#define LCD_GPIO_CNF_IN		(LCD_GPIO_CNF & ((uint32_t)0X8888))

#define LCD_BUS							((uint16_t)0x0F00)
#define LCD_BUS_OFFSET_MSB	((uint8_t)0x04)
#define LCD_BUS_OFFSET_LSB	((uint8_t)0x08)

#define E_0		(E_GPIO->ODR &= ~E_ODR)
#define RW_0	(RW_GPIO->ODR &= ~RW_ODR)
#define RS_0	(RS_GPIO->ODR &= ~RS_ODR)
#define E_1		(E_GPIO->ODR |= E_ODR)
#define RW_1	(RW_GPIO->ODR |= RW_ODR)
#define RS_1	(RS_GPIO->ODR |= RS_ODR)

#else

#define RS	(uint8_t)(1<<0)
#define RW	(uint8_t)(1<<1)
#define E		(uint8_t)(1<<2)
#define K		(uint8_t)(1<<3)
#define D4	(uint8_t)(1<<4)
#define D5	(uint8_t)(1<<5)
#define D6	(uint8_t)(1<<6)
#define D7	(uint8_t)(1<<7)

#define INSTRUCTION	(uint8_t)(~RS & 0x01)
#define DATA				(uint8_t)(RS & 0x01)
#define WRITE				(uint8_t)(~RW & 0x02)
#define READ				(uint8_t)(RW  & 0x02)
#define EN_LCD			(uint8_t)(E & 0x04)
#define DIS_LCD			(uint8_t)(~E & 0x04)
#define EN_BL				(uint8_t)(K	& 0x08)
#define DIS_BL			(uint8_t)(~K	& 0x08)

pcf8574_t *pcf;

#endif

static unsigned char reverse_table[16] =
{
	0x0, 0x8, 0x4, 0xC, 0x2, 0xA, 0x6, 0xE,
	0x1, 0x9, 0x5, 0xD, 0x3, 0xB, 0x7, 0xF
};
static uint8_t reverseBits(uint8_t halfByte)
{
	return (reverse_table[halfByte & 0x0F]);
}

static uint8_t setOutHalfData(uint8_t data, uint8_t type, uint8_t mode, uint8_t enable)
{
	uint8_t out;
	out = data << 0x04 | EN_BL | enable | mode | type;
	return out;
}

static void WriteCmdInit(uint8_t cmd)
{
	#ifndef LCD_I2C
	RW_0;	// ����� ������
	RS_0;	// �������
	LCD_OUT &= ~LCD_BUS;	// ����� ���� ������
	E_1;
	LCD_OUT |= (cmd&0xF0)<<LCD_BUS_OFFSET_MSB;	// ������ ���������
	delay_us(50);
	E_0;
	#else
	uint8_t lcdOutData;
	lcdOutData = setOutHalfData((cmd>>0x04)&0x0F, INSTRUCTION, WRITE, EN_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	Delay_us(50);
	lcdOutData = setOutHalfData((cmd>>0x04)&0x0F, INSTRUCTION, WRITE, DIS_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	#endif
}

static void SendData(uint8_t data, uint8_t type)
{
	#ifndef LCD_I2C
	if(type == INSTRUCTION)
		RS_0;
	else 
		RS_1;
	RW_0;	// ����� ������
	delay_us(1);
	LCD_OUT &= ~LCD_BUS;	// ����� ���� ������
	E_1;
	LCD_OUT |= (data&0xF0)<<LCD_BUS_OFFSET_MSB; // ������� �������� ������
	delay_us(50);
	E_0;
	delay_us(50);
	LCD_OUT &= ~LCD_BUS;
	E_1;
	LCD_OUT |= (data&0x0F)<<(LCD_BUS_OFFSET_LSB);	// ������� �������� ������
	delay_us(50);
	E_0;
	#else
	
	uint8_t lcdOutData;
	lcdOutData = setOutHalfData((data&0xF0)>>0x04, type, WRITE, EN_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	Delay_us(50);
	lcdOutData = setOutHalfData((data&0xF0)>>0x04, type, WRITE, DIS_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	Delay_us(50);
	
	lcdOutData = setOutHalfData(data&0x0F, type, WRITE, EN_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	Delay_us(50);
	lcdOutData = setOutHalfData(data&0x0F, type, WRITE, DIS_LCD);
	writeToPcf8574(pcf,&lcdOutData,1);
	Delay_us(50);
	
	#endif
}

static void CheckBusy(void)
{
	#ifndef LCD_I2C
	uint8_t busy_flag = 0;
	LCD_CONF &= ~(LCD_GPIO_MODE|LCD_GPIO_CNF);	// ����� ������ �����
	LCD_CONF |= LCD_GPIO_MODE_IN|LCD_GPIO_CNF_IN;	// ��������� ����� �� ���� � ���������
	LCD_OUT |= LCD_BUS;	// �������� � �������
	RW_1; // ����� ������
	RS_0;	// �������
	delay_us(1);
	do
	{
		E_1;
		delay_us(50);
		E_0;
		busy_flag = LCD_BUS&0x80;
		delay_us(50);
		E_1;
		delay_us(50);
		E_0;
		delay_us(50);
	} while(busy_flag);
	
	RW_0;	// ����� ������
	LCD_CONF &= ~(LCD_GPIO_MODE|LCD_GPIO_CNF);	// ����� ������ �����
	LCD_CONF |= LCD_GPIO_MODE_OUT|LCD_GPIO_CNF_OUT;	// ��������� ����� �� ����� � ���������
	#else
	uint8_t lcdOutData = 0xFF;
	while(lcdOutData & D7)
	{
		lcdOutData = setOutHalfData(0xFF, INSTRUCTION, READ, EN_LCD);
		writeToPcf8574(pcf,&lcdOutData,1);
		Delay_us(50);
		lcdOutData = setOutHalfData(0xFF, INSTRUCTION, READ, DIS_LCD);
		writeToPcf8574(pcf,&lcdOutData,1);
		Delay_us(50);
		readFromPcf8574(pcf,&lcdOutData,1);
		Delay_us(50);
	}
	#endif
}

void LcdWriteCmd(uint8_t data)
{
	//CheckBusy();
	//delay_ms(3);
	SendData(data, INSTRUCTION);
}

void LcdWriteData(uint8_t data)
{
//	CheckBusy();
	SendData(data,DATA);
}

void LcdWriteStr(char *str)
{
	while(*str)
	{
		LcdWriteData(*str++);
	}
}

void LcdSetCursorPos(uint8_t position)
{
	LcdWriteCmd(LCD_DDRAM_SET | position);
}

void LcdClear()
{
	LcdWriteCmd(LCD_CLEAR);
}

void LcdDisplayOn()
{
	LcdWriteCmd(LCD_DISP | LCD_DISP_D);
	LcdBacklightOn();
}

void LcdDisplayOff()
{
	LcdWriteCmd(LCD_DISP);
	LcdBacklightOff();
}

void LcdBacklightOn()
{
	uint8_t backlight = EN_BL;
	writeToPcf8574(pcf, &backlight, 1);
}

void LcdBacklightOff()
{
	uint8_t backlight = DIS_BL;
	writeToPcf8574(pcf, &backlight, 1);
}

void LcdInit(pcf8574_t *pPCF)
{
	pcf = pPCF;
	Delay_ms(100);
	WriteCmdInit(0x30);
	Delay_ms(5);
	WriteCmdInit(0x30);
	Delay_us(100);
	WriteCmdInit(0x30);
	Delay_ms(1);
	//��������� ����������, ���������� �����������
	WriteCmdInit(0x20);	// DL=0(4������ ����)
	WriteCmdInit(0x20);	// ���������: DL=0(4������ ����)
	WriteCmdInit(0xC0);	// N=1(��� ������) F=0(5*8 �����)
	//WriteCmdInit(0x24);	// N=0(��� ������) F=0(5*8 �����)
	Delay_us(50);
	//��������� ������ �����������
	WriteCmdInit(0x00);
	WriteCmdInit(0xF0);	//D=1(�������� �������), �=1(�������� ������ � ���� ��������), B=0(�� �������� �������)
	//WriteCmdInit(0xC0);	//D=1(�������� �������), �=0, B=0(��� �������)
	Delay_us(50);
	//�������� �������
	WriteCmdInit(0x00);
	WriteCmdInit(0x10);
	Delay_ms(2);
	WriteCmdInit(0x00);
	WriteCmdInit(0x60);	//ID=1(��������� �������� ������),S=0(����� �� ��������)
}
