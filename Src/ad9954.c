#include "ad9954.h"
#include "spi.h"
#include "gltime.h"
#include "stdlib.h"
#include "math.h"

#define TIMEOUT 1

static uint32_t calcFtw(uint32_t outFreq, uint32_t sysFreq)
{
	uint32_t FTW = 0;
	if(outFreq > 200000000)
	{
		outFreq = 200000000;
	}
	FTW = (outFreq * 4294967296) / sysFreq ;
	return FTW;
}

static LL_RSCW_t AD9954_setRscwReg(RSCW_t *rscw)
{
	LL_RSCW_t LL_rscw;
	LL_rscw.ram_segment_beginning_addr_H = (uint8_t)(rscw->ram_segment_beginning_addr >> 6) & 0x0F;
	LL_rscw.ram_segment_beginning_addr_L = (uint8_t)(rscw->ram_segment_beginning_addr) & 0x3F;
	LL_rscw.no_dwell_active = rscw->no_dwell_active;
	LL_rscw.ram_segment_mode_ctrl = rscw->ram_segment_mode_ctrl;
	LL_rscw.ram_segment_final_addr_H = (uint8_t)(rscw->ram_segment_final_addr >> 8) & 0x03;
	LL_rscw.ram_segment_final_addr_L = (uint8_t)(rscw->ram_segment_final_addr);
	LL_rscw.ram_segment_addr_ramp_rate_H = (uint8_t)(rscw->ram_segment_addr_ramp_rate >> 8);
	LL_rscw.ram_segment_addr_ramp_rate_L = (uint8_t)(rscw->ram_segment_addr_ramp_rate);
	return LL_rscw;
}

static RSCW_t AD9954_getRscwReg(LL_RSCW_t *LL_rscw)
{
	RSCW_t rscw;
	rscw.ram_segment_beginning_addr = ((uint16_t)LL_rscw->ram_segment_beginning_addr_H << 6) | (uint16_t)LL_rscw->ram_segment_beginning_addr_L;
	rscw.no_dwell_active = LL_rscw->no_dwell_active;
	rscw.ram_segment_mode_ctrl = LL_rscw->ram_segment_mode_ctrl;
	rscw.ram_segment_final_addr = ((uint16_t)LL_rscw->ram_segment_final_addr_H << 8) | LL_rscw->ram_segment_final_addr_L;
	rscw.ram_segment_addr_ramp_rate = ((uint16_t)LL_rscw->ram_segment_addr_ramp_rate_H) << 8 | LL_rscw->ram_segment_addr_ramp_rate_L;
	return rscw;
}

void AD9954_generateSweepTable(ad9954_t *ad9954, uint32_t freqCarrier, uint32_t freqSweep, uint32_t *table, uint16_t lenTab, AD9954_TYPE_SWEEP_TABLE sweepType)
{
	const double dPI = 6.28318530717959;
	uint32_t freqMin = freqCarrier - freqSweep;
	uint32_t freqMax = freqCarrier + freqSweep;

	uint32_t koeff = (freqMax - freqMin)/2;

	for(uint16_t i = 0; i < lenTab; i++)
	{
		double Fo;
		double tmp1 = (double)i/(double)lenTab;
		if(sweepType == TYPE_SINE)
		{
			Fo = koeff + freqMin + koeff * sin(dPI * tmp1);
		}
		else if(sweepType == TYPE_LINEAR)
		{
			Fo = freqMax * tmp1 + freqMin;
		}
		table[i] = calcFtw(Fo, ad9954->systemFrequency);
	}
}


void AD9954_setSystemFreq(ad9954_t *ad9954)
{
	uint32_t mult = ad9954->systemFrequency / ad9954->xtalFrequency;
	CFR2_t regSettings;
	regSettings.refclk_multiplier = mult;
	if(ad9954->systemFrequency > 250000000)
	{
		regSettings.vco_range = 1;
	}
	else
	{
		regSettings.vco_range = 0;
	}
	AD9954_writeRegister(ad9954,AD9954_CFR2,(uint8_t*)&(regSettings.cfr2),3);
	AD9954_updateIO(ad9954);
}

void AD9954_setSingleFreq(ad9954_t *ad9954, uint32_t freq)
{
	uint32_t FTW = calcFtw(freq,ad9954->systemFrequency);
	AD9954_writeRegister(ad9954, AD9954_FTW0, (uint8_t*)&FTW, 4);
	AD9954_updateIO(ad9954);
}

void AD9954_setPhaseOffset(ad9954_t *ad9954, uint16_t phaseOffset)
{
	uint16_t POW = (phaseOffset * 16384) / 360;
	POW &= 0x3FF;
	AD9954_writeRegister(ad9954, AD9954_POW0, (uint8_t*)&POW, 2);
	AD9954_updateIO(ad9954);
}
	
void AD9954_setSweepFreq(ad9954_t *ad9954, uint32_t freq_0, uint32_t freq_1, uint32_t posDF, uint8_t posRR, uint32_t negDF, uint8_t negRR)
{
	uint32_t FTW0 = calcFtw(freq_0,ad9954->systemFrequency);
	uint32_t FTW1 = calcFtw(freq_1,ad9954->systemFrequency);
	uint32_t syncClkFreq = ad9954->systemFrequency / 4;
	uint32_t deltaFreq = abs((int32_t)(freq_1 - freq_0));
	
//	uint32_t risingSteps = syncClkFreq / posDF;
//	uint32_t fallingSteps = syncClkFreq / negDF;
//	uint32_t risingDeltaFreq1 = deltaFreq / risingSteps;
//	uint32_t fallingDeltaFreq1 = deltaFreq / fallingSteps;

	uint32_t risingDeltaFreq = (deltaFreq * posDF) / syncClkFreq;
	uint32_t fallingDeltaFreq = (deltaFreq * negDF) / syncClkFreq;
	
	NLSCW_t nlscw;
	nlscw.falling_delta_freq_tun_word = calcFtw(fallingDeltaFreq,ad9954->systemFrequency);
	nlscw.falling_sweep_ramp_rate = 0x01;
	PLSCW_t plscw;
	plscw.rising_delta_freq_tun_word = calcFtw(risingDeltaFreq,ad9954->systemFrequency);
	plscw.rising_sweep_ramp_rate = 0x01;
	
	AD9954_writeRegister(ad9954, AD9954_PLSCW, (uint8_t*)&(plscw.plscw), 5);
	AD9954_writeRegister(ad9954, AD9954_NLSCW, (uint8_t*)&(nlscw.nlscw), 5);
	AD9954_writeRegister(ad9954, AD9954_FTW0, (uint8_t*)&FTW0, 4);
	AD9954_writeRegister(ad9954, AD9954_FTW1, (uint8_t*)&FTW1, 4);
	AD9954_updateIO(ad9954);
}
	
void AD9954_setOperationMode(ad9954_t *ad9954, AD9954_MODES mode)
{
	CFR1_t regSettings;
	AD9954_readRegister(ad9954, AD9954_CFR1, (uint8_t*)&(regSettings.cfr1),4);
	
	switch((uint8_t)mode)
	{
		case SINGLE_TONE:
		{
			regSettings.linear_sweep_enable = 0;
			regSettings.ram_enable = 0;
			break;
		}
		case LINEAR_SWEEP:
		{
			regSettings.linear_sweep_enable = 1;
			regSettings.ram_enable = 0;
			break;
		}
		case RAM_MODE:
		{
			regSettings.linear_sweep_enable = 0;
			regSettings.ram_enable = 1;
			break;
		}
		default:
		{
			regSettings.linear_sweep_enable = 0;
			regSettings.ram_enable = 0;
			break;
		}
	}
	AD9954_writeRegister(ad9954, AD9954_CFR1, (uint8_t*)&regSettings.cfr1, 4);
	AD9954_updateIO(ad9954);
}
	
//void AD9954_setRamMode(ad9954_t *ad9954, AD9954_RAM_MODES ramMode);
	
void AD9954_setRamProfileSettings(ad9954_t *ad9954, AD9954_RAM_PROFILES profile, RSCW_t *profSettings)
{
	LL_RSCW_t rscwSettings = AD9954_setRscwReg(profSettings);
	AD9954_writeRegister(ad9954, AD9954_RSCW0 + profile, (uint8_t*)&(rscwSettings.rscw), 5);
	AD9954_updateIO(ad9954);
}

void AD9954_setRamInternalProfile(ad9954_t *ad9954, AD9954_RAM_INTERNAL_PROFILES profile)
{
	CFR1_t regSettings;
	AD9954_readRegister(ad9954, AD9954_CFR1, (uint8_t*)&(regSettings.cfr1),4);
	regSettings.inter_profile_ctrl = 0;
	AD9954_writeRegister(ad9954, AD9954_CFR1, (uint8_t*)&regSettings.cfr1, 4);
	AD9954_updateIO(ad9954);
}
	
void AD9954_setRamTable(ad9954_t *ad9954, AD9954_RAM_PROFILES profile, uint32_t *table, uint8_t tableLenInWord)
{
	switch((uint8_t)profile)
	{
		case PROFILE_0:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin << 16;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin << 16;
			break;
		}
		case PROFILE_1:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin << 16;
			break;
		}
		case PROFILE_2:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin << 16;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin;
			break;
		}
		case PROFILE_3:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin;
			break;
		}
	}
	LL_RSCW_t rscwSettingsLL;
	AD9954_readRegister(ad9954, AD9954_RSCW0 + profile, (uint8_t*)&(rscwSettingsLL.rscw),5);
	RSCW_t profileSettings = AD9954_getRscwReg(&rscwSettingsLL);
	uint32_t lenSegmentInWord = profileSettings.ram_segment_final_addr - profileSettings.ram_segment_beginning_addr + 1;
	if(tableLenInWord > lenSegmentInWord)
	{
		tableLenInWord = lenSegmentInWord;
	}
	//������ ������� � RAM
	uint32_t lenTableInByte = tableLenInWord * 4;
	uint8_t instruction_byte = (AD9954_RAM & A9954_INSTUCTION_MASK) | AD9954_WRITE;
	RESET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
	WriteByteSPI(ad9954->pSPI, instruction_byte);
	WriteDataSPI(ad9954->pSPI, (uint8_t*)table, lenTableInByte, 1);
	Delay_us(10);
	SET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
	AD9954_updateIO(ad9954);
}

void AD9954_setProfile(ad9954_t *ad9954, AD9954_RAM_PROFILES profile)
{
		switch((uint8_t)profile)
	{
		case PROFILE_0:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin << 16;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin << 16;
			break;
		}
		case PROFILE_1:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin << 16;
			break;
		}
		case PROFILE_2:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin << 16;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin;
			break;
		}
		case PROFILE_3:
		{
			ad9954->PS0Port->BSRR = ad9954->PS0Pin;
			ad9954->PS1Port->BSRR = ad9954->PS1Pin;
			break;
		}
	}
}

/*Low-Level commands*/
void AD9954_writeRegister(ad9954_t *ad9954, uint8_t regAddr, uint8_t *data, uint8_t dataLen)
{
	
	uint8_t instruction_byte = (regAddr & A9954_INSTUCTION_MASK) | AD9954_WRITE;
	RESET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
	
//	GPIOB->MODER &= ~GPIO_MODER_MODE15;
//	GPIOB->MODER |= GPIO_MODER_MODE15_0;
//	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD15;
//	
//	for(uint8_t i=0; i<8; i++)
//	{
//		GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK
//		if(instruction_byte & 0x80)
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BS15; //MOSI
//		}
//		else
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BR15; //MOSI
//		}
//		Delay_us(TIMEOUT);
//		GPIOB->BSRR |= GPIO_BSRR_BS13; //SCK
//		Delay_us(TIMEOUT);
//		instruction_byte <<= 1;
//	}
//	for(uint8_t j=dataLen; j > 0; j--)
//	{
//		uint8_t dat = data[j-1];
//		for(uint8_t i=0; i<8; i++)
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK
//			if(dat & 0x80)
//			{
//				GPIOB->BSRR |= GPIO_BSRR_BS15; //MOSI
//			}
//			else
//			{
//				GPIOB->BSRR |= GPIO_BSRR_BR15; //MOSI
//			}
//			Delay_us(TIMEOUT);
//			GPIOB->BSRR |= GPIO_BSRR_BS13; //SCK
//			Delay_us(TIMEOUT);
//			dat <<= 1;
//		}
//	}
//	GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK

	WriteByteSPI(ad9954->pSPI, instruction_byte);
	WriteDataSPI(ad9954->pSPI, data, dataLen, 1);
	Delay_us(10);
	SET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
}
void AD9954_readRegister(ad9954_t *ad9954, uint8_t regAddr, uint8_t *data, uint8_t dataLen)
{
	uint8_t instruction_byte = (regAddr & A9954_INSTUCTION_MASK) | AD9954_READ;
	RESET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
	
//	GPIOB->MODER &= ~GPIO_MODER_MODE15;
//	GPIOB->MODER |= GPIO_MODER_MODE15_0;
//	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD15;
//	
//	for(uint8_t i=0; i<8; i++)
//	{
//		GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK
//		
//		if(instruction_byte & 0x80)
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BS15; //MOSI
//		}
//		else
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BR15; //MOSI
//		}
//		Delay_us(TIMEOUT);
//		GPIOB->BSRR |= GPIO_BSRR_BS13; //SCK
//		Delay_us(TIMEOUT);
//		instruction_byte <<= 1;
//	}
//	
//	
//	GPIOB->MODER &= ~GPIO_MODER_MODE15;
////	GPIOB->PUPDR |= GPIO_PUPDR_PUPD15_1;
//	
//	GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK
//	Delay_us(TIMEOUT);
//	for(uint8_t j=dataLen; j > 0; j--)
//	{
//		uint8_t dat = 0;
//		for(uint8_t i=0; i<8; i++)
//		{
//			GPIOB->BSRR |= GPIO_BSRR_BS13; //SCK
//			dat |= GPIOB->IDR & GPIO_IDR_ID15;
//			dat <<= 1;
//			Delay_us(TIMEOUT);
//			GPIOB->BSRR |= GPIO_BSRR_BR13; //SCK
//			Delay_us(TIMEOUT);
//		}
//		data[j-1] = dat;
//	}

	WriteByteSPI(ad9954->pSPI, instruction_byte);
	ReadDataSPI(ad9954->pSPI, data, dataLen, 1);
	Delay_us(10);
	SET_CS(ad9954->CSPort->BSRR, ad9954->CSPin);
}

void AD9954_updateIO(ad9954_t *ad9954)
{
	ad9954->IOUPDPort->BSRR |= ad9954->IOUPDPin;
	ad9954->IOUPDPort->BSRR |= ad9954->IOUPDPin << 16;
}
