#include "InitRCC.h"

uint8_t InitRCC(void) //��������� �������� ������
{

  uint32_t StartUpCounter = 0, HSEStatus = 0;

  /* ������������  SYSCLK, HCLK, PCLK2 � PCLK1 */
  /* �������� HSE */
  RCC->CR |= ((uint32_t)RCC_CR_HSEON);

  /* ���� ���� HSE �� �������� ��� ���������� ���� �� ������ �������*/
  do 
  {
    HSEStatus = RCC->CR & RCC_CR_HSERDY;
    StartUpCounter++;
  } 
  while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

  if ((RCC->CR & RCC_CR_HSERDY) != RESET) 
  {
    HSEStatus = (uint32_t)0x01;
  }
  else 
  {
    HSEStatus = (uint32_t)0x00;
  }
    /* ���� HSE ���������� ��������� */
  if ( HSEStatus == (uint32_t)0x01) 
  {
    /* �������� ����� ����������� FLASH */
    FLASH->ACR |= FLASH_ACR_PRFTEN;
    /* ������������� Flash �� 5 ������ �������� */
    /* ��� ����� ������, ��� Flash �� ����� �������� �� ������� ������� */
    /* ���� ��� �� �������, �� ����� �������� ����. ���� ����� �����������, �� ����� ����*/
    /* ������ �������� ��� "������� ������". ��� ����� ��� ����������� ����.*/
    FLASH->ACR &= (~FLASH_ACR_LATENCY);
    FLASH->ACR |= FLASH_ACR_LATENCY_5WS;

    /* HCLK = SYSCLK */
    /* AHB Prescaler = 1 */
    RCC->CFGR |= RCC_CFGR_HPRE_DIV1;		
   
    /* PCLK2 = HCLK/1 */
    /* APB2 Prescaler = 1 */
		RCC->CFGR |= RCC_CFGR_PPRE2_DIV1;
   
    /* PCLK1 = HCLK/2 */
    /* APB1 Prescaler = 2 */
		RCC->CFGR |= RCC_CFGR_PPRE1_DIV2;
   
    /* ������������� ��������� PLL */
   
    /* ���������� � ���� ������� ��������*/
    RCC->PLLCFGR &= ~(RCC_PLLCFGR_PLLSRC | RCC_PLLCFGR_PLLM | RCC_PLLCFGR_PLLN | RCC_PLLCFGR_PLLP);
   
    /* � ������ ���������� ����� */
    /* RCC_PLLCFGR_PLLSRC_HSE -- �������� HSE �� ���� */
    /* 0x0F<<RCC_PLLCFGR_PLLM_Pos -- ����� ������� �� 15
        0x90<<RCC_PLLCFGR_PLLN_Pos -- �������� �� 144 (������� PLLN)
        RCC_PLLCFGR_PLLP_0 PLLP -- ����v �� 4 
				0x05 << RCC_PLLCFGR_PLLQ_Pos -- PLLQ ����� �� 5 ��� USB*/
    /* � ����� ����� �������: HSE -- 25 ���
       PLLM 25/15, PLLN 1,6x144, PLLP 240/4
       SYSCLK==HCLK==60 ���
    */
    RCC->PLLCFGR |= RCC_PLLCFGR_PLLSRC_HSE | (0x0F<<RCC_PLLCFGR_PLLM_Pos) | (0x90 << RCC_PLLCFGR_PLLN_Pos) | RCC_PLLCFGR_PLLP_0 | (0x05 << RCC_PLLCFGR_PLLQ_Pos);

    /* ��� ���������? �������� PLL */
    RCC->CR |= RCC_CR_PLLON;
   
      /* �������, ���� PLL �������� ��� ���������� */
    while((RCC->CR & RCC_CR_PLLRDY) == 0) {}
   
    /* ��������? ����� �����������! �������� PLL ��� �������� ��������� ������� */
    RCC->CFGR &= ~(RCC_CFGR_SW);
    RCC->CFGR |= RCC_CFGR_SW_PLL;
   
    /* �������, ���� PLL ��������� ��� �������� ��������� ������� */
      while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS) != RCC_CFGR_SWS_1) {}
  }
  else 
  {
  /* ��� �����... HSE �� �������... ����-�� � ������� ��� ��� ���...
  ���� �� ����� ���������� ��� ������... ���� �� �����, �� �� ��������
  �� HSI! */
  }

  return HSEStatus;
}
