#include "stm32f10x.h"
#include "menu.h"
#include "LCD.h"
#include "delay.h"
#include "RTC.h"
#include "temp_hum.h"


/*
*
* ������� menu() ������������ ����� ���� �� ��������� � ��������� ������� ������ ���������� ��� ���������.
*
*/
static inline void InitMenu(void);
static inline void PrintMenu(char*, char*);
static inline void AutoModeHandler(void);
static inline void ManualModeHandler(void);
static inline void PrintTemp(char*, char*);
static void HistTempHandler(void);
static inline void PrintHum(char*);
static void HistHumHandler(void);
static void HistErrHandler(void);
static void HistOnHandler(void);
static void HistOffHandler(void);
static inline void ClrErrHandler(void);
static void TimeSetupHandler(void);

typedef struct menu
{
	unsigned char id;	//ID ������ ����
	char *name1;	//������ ������ ����� ����
	char *name2;	//������ ������ ����� ����
	struct menu *parent;	//��������� �� ������������ ������� (�������)
	struct menu *child;	//��������� �� �������� ������� (�������)
	struct menu *next;	//��������� �� �������� ������� ������
	struct menu *prev;	//��������� �� �������� ������� �����
	void (*handler)();	//��������� �� �������-���������� ������ ����
} menuItem;


menuItem 	mod =				{1,	"Mode",					"\0",						0,	0,	0,	0,	0};	//����� "�����" �������� ����
menuItem 	mod1	=			{11, "Automatic", 	"\0",						0,	0,	0,	0,	0};	//����� "������ ����������" ���� "�����"
menuItem 	mod2 = 			{12, "Manual",			"\0",						0,	0,	0,	0,	0};	//����� "�������������� ����������" ���� "�����"
menuItem 	temp_hist =	{2,	"History of",		"temperature",	0,	0,	0,	0,	0};	//����� "������� �����������" �������� ����
menuItem 	hum_hist =	{3,	"History of",		"humidity",			0,	0,	0,	0,	0};	//����� "������� ���������" �������� ����
menuItem 	err_hist =	{4,	"History of",		"errors",				0,	0,	0,	0,	0};	//����� "������� ������" �������� ����
menuItem	on_hist	=		{5,	"History of",		"starts",				0,	0,	0,	0,	0};	//����� "������� ��������" �������� ����
menuItem	off_hist	=	{6,	"History of",		"stops",				0,	0,	0,	0,	0};	//����� "������� ���������" �������� ����
menuItem 	err_clr =		{7,	"Clear errors",	"\0",						0,	0,	0,	0,	0};	//����� "������� ������" �������� ����
menuItem 	setup =			{8,	"Time setup",		"\0",						0,	0,	0,	0,	0};	//����� "��������� �������" �������� ����
	
static inline void InitMenu()
{
	mod.child =	&mod1;	//��������� �� ��������
	mod.next =	&temp_hist;	//��������� �� ������ ������
	mod.prev =	&setup;	//��������� �� ������ �����
	mod.parent =&mod;	//��������� �� ��������(��������). ��� ������� �������� ������ ��������� ��� �� ����

	mod1.child =	&mod1;
	mod1.next =		&mod2;
	mod1.prev =		&mod2;
	mod1.parent =	&mod;
	mod1.handler =	AutoModeHandler;
		
	mod2.child = 	&mod2;
	mod2.next = 	&mod1;
	mod2.prev = 	&mod1;
	mod2.parent = &mod;
	mod2.handler =	ManualModeHandler;
		
	temp_hist.child =	&temp_hist;
	temp_hist.next =	&hum_hist;
	temp_hist.prev =	&mod;
	temp_hist.parent =&temp_hist;
	temp_hist.handler =	HistTempHandler;

	hum_hist.child =	&hum_hist;
	hum_hist.next =		&err_hist;
	hum_hist.prev =		&temp_hist;
	hum_hist.parent =	&hum_hist;
	hum_hist.handler	=	HistHumHandler;

	err_hist.child =	&err_hist;
	err_hist.next =		&on_hist;
	err_hist.prev =		&hum_hist;
	err_hist.parent =	&err_hist;
	err_hist.handler	=	HistErrHandler;
	
	on_hist.child	=	&on_hist;
	on_hist.next	=	&off_hist;
	on_hist.prev	=	&err_hist;
	on_hist.parent	=	&on_hist;
	on_hist.handler	=	HistOnHandler;
	
	off_hist.child	=	&off_hist;
	off_hist.next	=	&err_clr;
	off_hist.prev	=	&on_hist;
	off_hist.parent	=	&off_hist;
	off_hist.handler	=	HistOffHandler;

	err_clr.child =		&err_clr;
	err_clr.next =		&setup;
	err_clr.prev =		&err_hist;
	err_clr.parent =	&err_clr;
	err_clr.handler	=	ClrErrHandler;

	setup.child =		&setup;
	setup.next =		&mod;
	setup.prev =		&err_clr;
	setup.parent =	&setup;
	setup.handler = TimeSetupHandler;
}

static inline void PrintMenu(char *str1, char *str2)	// ����������� ���� �� ������
{
	WriteCmdLCD(LCD_CLEAR);
	delay_ms(2);
	WriteStrLCD(str1);
	WriteCmdLCD(LCD_DDRAM_SET|0x40);
	delay_ms(2);
	WriteStrLCD(str2);
}

struct 
{
	uint8_t Enter_old;
	uint8_t Right_old;
	uint8_t Left_old;
	uint8_t Esc_old;
}btn;

static uint8_t ButtonSelect()	// �������� ������� ������ 
{
	delay_ms(100);
	if(!Enter && btn.Enter_old)
	{
		btn.Enter_old = 0;
		return 0;
	}
	if(Enter && !(btn.Enter_old))
	{
		btn.Enter_old = 1;
	}
	//---
	if(!Right && btn.Right_old)
	{
		btn.Right_old = 0;
		return 1;
	}
	if(Right && !(btn.Right_old))
	{
		btn.Right_old = 1;
	}
	//---
	if(!Left && btn.Left_old)
	{
		btn.Left_old = 0;
		return 2;
	}
	if(Left && !(btn.Left_old))
	{
		btn.Left_old = 1;
	}
	//---
	if(!Esc && btn.Esc_old)
	{
		btn.Esc_old = 0;
		return 3;
	}
	if(Esc && !(btn.Esc_old))
	{
		btn.Esc_old = 1;
	}
	return 4;
}

enum {ENTER=0,	RIGHT, LEFT, ESC} button_status;

void menu()
{
	InitMenu();
	uint8_t flag_menu=1;
	menuItem *curMenu = &mod;	//��������� �������� ������ ����
	PrintMenu(curMenu->name1, curMenu->name2);	// ���������� ������� ����� ����
	btn.Enter_old =	0;
	btn.Right_old =	0;
	btn.Left_old =	0;
	btn.Esc_old =		0;
	while(flag_menu)	// ���� ����=1 �������� � ����
	{
		button_status = ButtonSelect();	// �������� ������� ������, ���� ������
		switch (button_status)
    {
    	case ENTER:	// ���� ������ ENTER
			{
				if (curMenu == curMenu->child)	// ���� ����� �� ����� ����������
        {
					if(curMenu->handler)	//� ����� �������-����������
					{
						curMenu->handler();	//����� �����������
						curMenu = curMenu->parent;	//����� �� ������� ����
						PrintMenu(curMenu->name1, curMenu->name2);
					}
        }
        else	//���� ��������� ����
        {
					curMenu = curMenu->child; //������� �� ��������
					PrintMenu(curMenu->name1, curMenu->name2);
        }
    		break;
			}
    	case RIGHT:
			{
				curMenu = curMenu->next;	//������� �� ������ ������
				PrintMenu(curMenu->name1, curMenu->name2);
    		break;
			}
			case LEFT:
			{
				curMenu = curMenu->prev;	//������� �� ������ �����
				PrintMenu(curMenu->name1, curMenu->name2);
    		break;
			}
			case ESC:
			{
				if (curMenu == curMenu->parent)	//���� ������� ����� - ����� �������� ����
        {
					flag_menu = 0;	//���� ������ �� ����
        }
        else
        {
					curMenu = curMenu->parent;	//����� ������� � ��������
					PrintMenu(curMenu->name1, curMenu->name2);
        }
    		break;
			}
    	default:
    		break;
    }//end switch
	}//end wile
}

static inline void AutoModeHandler()	//���������� ��������� ��������������� ������
{
	extern uint8_t ctrl_mode;
	ctrl_mode = 0;
}

static inline void ManualModeHandler()	//���������� ��������� ������� ������
{
	extern uint8_t ctrl_mode;
	ctrl_mode = 1;
}
static inline void PrintTemp(char* Pog_str, char* Gar_str)
{
	WriteCmdLCD(LCD_CLEAR);
	delay_ms(2);
	WriteStrLCD("TEMP P ");
	WriteStrLCD(Pog_str);
	WriteCmdLCD(LCD_DDRAM_SET|0x40);
	delay_ms(2);
	WriteStrLCD("TEMP G ");
	WriteStrLCD(Gar_str);
}
static void HistTempHandler()	//���������� ����������� ������� �����������
{
	extern uint16_t tempP_hist_buff[5];
	extern uint8_t tempG_hist_buff[5];
	extern uint8_t temp_hist_index;
	char tempP_str[8] = {0}, tempG_str[6] = {0};
	uint8_t flag_hist = 1;
	uint8_t index;
		if(temp_hist_index)
	{
		 index = temp_hist_index-1;
	}
	else
	{
		index = temp_hist_index;
	}
	Temp2Str(tempP_hist_buff[index],tempP_str,1);
	Temp2Str(tempG_hist_buff[index],tempP_str,0);
	PrintTemp(tempP_str,tempG_str);

	while(flag_hist)
	{
		button_status = ButtonSelect();
		switch(button_status)
		{
			case RIGHT:
			{
				if((index < 4) && (index != temp_hist_index))
				{
					index ++;
					Temp2Str(tempP_hist_buff[index],tempP_str,1);
					Temp2Str(tempG_hist_buff[index],tempP_str,0);
					PrintTemp(tempP_str,tempG_str);
				}
				else if((index == 4) && (index != temp_hist_index))
				{
					index = 0;
					Temp2Str(tempP_hist_buff[index],tempP_str,1);
					Temp2Str(tempG_hist_buff[index],tempP_str,0);
					PrintTemp(tempP_str,tempG_str);
				}
				break;
			} //end case RIGTH
			case LEFT:
			{
				if((index > 0) && (index-1 != temp_hist_index))
				{
					index --;
					Temp2Str(tempP_hist_buff[index],tempP_str,1);
					Temp2Str(tempG_hist_buff[index],tempP_str,0);
					PrintTemp(tempP_str,tempG_str);
				} 
				else if(!index)
				{
					index = 4;
					Temp2Str(tempP_hist_buff[index],tempP_str,1);
					Temp2Str(tempG_hist_buff[index],tempP_str,0);
					PrintTemp(tempP_str,tempG_str);
				}
				break;
			} //end case RIGTH
			case ESC:
			{
				flag_hist = 0;
				break;
			}	//end case ESC
		} //end switch(button_status)
	} //end while
}

static inline void PrintHum(char* humid_str)
{
	WriteCmdLCD(LCD_CLEAR);
	delay_ms(2);
	WriteStrLCD("HUMIDITY ");
	WriteStrLCD(humid_str);
}

static void HistHumHandler()	//���������� ����������� ������� ���������
{
	extern uint16_t hum_hist_buff[5];
	extern uint8_t hum_hist_index;
	char hum_str[6] = {0};
	uint8_t flag_hist = 1;
	uint8_t index;
	if(hum_hist_index)
	{
		 index = hum_hist_index-1;
	}
	else
	{
		index = hum_hist_index;
	}
	Hum2Str(hum_hist_buff[index],hum_str);
	PrintHum(hum_str);

	while(flag_hist)
	{
		button_status = ButtonSelect();
		switch(button_status)
		{
			case RIGHT:
			{
				if((index < 4) && (index != hum_hist_index))
				{
					index ++;
					Hum2Str(hum_hist_buff[index],hum_str);
					PrintHum(hum_str);
				}
				else if((index == 4) && (index != hum_hist_index))
				{
					index = 0;
					Hum2Str(hum_hist_buff[index],hum_str);
					PrintHum(hum_str);
				}
				break;
			} //end case RIGTH
			case LEFT:
			{
				if((index > 0) && (index-1 != hum_hist_index))
				{
					index --;
					Hum2Str(hum_hist_buff[index],hum_str);
					PrintHum(hum_str);
				} 
				else if(!index)
				{
					index = 4;
					Hum2Str(hum_hist_buff[index],hum_str);
					PrintHum(hum_str);
				}
				break;
			} //end case RIGTH
			case ESC:
			{
				flag_hist = 0;
				break;
			}	//end case ESC
		} //end switch(button_status)
	} //end while
}

static void HistErrHandler()	//���������� ����������� ������� ������� ������
{
	extern RTCtime_t err_hist_buff[3];
	extern uint8_t err_hist_index;
	char err_str[]={"DD-MM-YYYY hh:mm:ss"};
	uint8_t flag_hist = 1;
	uint8_t index;
	if(err_hist_index)
	{
		 index = err_hist_index-1;
	}
	else
	{
		index = err_hist_index;
	}
	if(!err_hist_buff[index].year)
	{
		PrintMenu("NO", "ERROR");
	}
	else
	{
		TimeToStr(err_str, &err_hist_buff[index]);
		PrintMenu(err_str, &err_str[11]);
	}	

	while(flag_hist)
	{
		button_status = ButtonSelect();
		switch(button_status)
		{
			case RIGHT:
			{
				if((index < 2) && (index != err_hist_index))
				{
					index ++;
					if(!err_hist_buff[index].year)
					{
						PrintMenu("NO", "ERROR");
					}
					else
					{
						TimeToStr(err_str, &err_hist_buff[index]);
						PrintMenu(err_str, &err_str[11]);
					}
				}
				else if((index == 2) && (index != err_hist_index))
				{
					index = 0;
					if(!err_hist_buff[index].year)
					{
						PrintMenu("NO", "ERROR");
					}
					else
					{
						TimeToStr(err_str, &err_hist_buff[index]);
						PrintMenu(err_str, &err_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case LEFT:
			{
				if((index > 0) && (index-1 != err_hist_index))
				{
					index --;
					if(!err_hist_buff[index].year)
					{
						PrintMenu("NO", "ERROR");
					}
					else
					{
						TimeToStr(err_str, &err_hist_buff[index]);
						PrintMenu(err_str, &err_str[11]);
					}
				} 
				else if(!index)
				{
					index = 2;
					if(!err_hist_buff[index].year)
					{
						PrintMenu("NO", "ERROR");
					}
					else
					{
						TimeToStr(err_str, &err_hist_buff[index]);
						PrintMenu(err_str, &err_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case ESC:
			{
				flag_hist = 0;
				break;
			}	//end case ESC
		} //end switch(button_status)
	} //end while
}

static void HistOnHandler()
{
	extern RTCtime_t on_hist_buff[3];
	extern uint8_t on_hist_index;
	char on_str[]={"DD-MM-YYYY hh:mm:ss"};
	uint8_t flag_hist = 1;
	uint8_t index;
	if(on_hist_index)
	{
		 index = on_hist_index-1;
	}
	else
	{
		index = on_hist_index;
	}
	if(!on_hist_buff[index].year)
	{
		PrintMenu("NO", "DATA");
	}
	else
	{
		TimeToStr(on_str, &on_hist_buff[index]);
		PrintMenu(on_str, &on_str[11]);
	}	

	while(flag_hist)
	{
		button_status = ButtonSelect();
		switch(button_status)
		{
			case RIGHT:
			{
				if((index < 2) && (index != on_hist_index))
				{
					index ++;
					if(!on_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(on_str, &on_hist_buff[index]);
						PrintMenu(on_str, &on_str[11]);
					}
				}
				else if((index == 2) && (index != on_hist_index))
				{
					index = 0;
					if(!on_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(on_str, &on_hist_buff[index]);
						PrintMenu(on_str, &on_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case LEFT:
			{
				if((index > 0) && (index-1 != on_hist_index))
				{
					index --;
					if(!on_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(on_str, &on_hist_buff[index]);
						PrintMenu(on_str, &on_str[11]);
					}
				} 
				else if(!index)
				{
					index = 2;
					if(!on_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(on_str, &on_hist_buff[index]);
						PrintMenu(on_str, &on_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case ESC:
			{
				flag_hist = 0;
				break;
			}	//end case ESC
		} //end switch(button_status)
	} //end while
}


static void HistOffHandler()
{
	extern RTCtime_t off_hist_buff[3];
	extern uint8_t off_hist_index;
	char off_str[]={"DD-MM-YYYY hh:mm:ss"};
	uint8_t flag_hist = 1;
	uint8_t index;
	if(off_hist_index)
	{
		 index = off_hist_index-1;
	}
	else
	{
		index = off_hist_index;
	}
	if(!off_hist_buff[index].year)
	{
		PrintMenu("NO", "DATA");
	}
	else
	{
		TimeToStr(off_str, &off_hist_buff[index]);
		PrintMenu(off_str, &off_str[11]);
	}	

	while(flag_hist)
	{
		button_status = ButtonSelect();
		switch(button_status)
		{
			case RIGHT:
			{
				if((index < 2) && (index != off_hist_index))
				{
					index ++;
					if(!off_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(off_str, &off_hist_buff[index]);
						PrintMenu(off_str, &off_str[11]);
					}
				}
				else if((index == 2) && (index != off_hist_index))
				{
					index = 0;
					if(!off_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(off_str, &off_hist_buff[index]);
						PrintMenu(off_str, &off_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case LEFT:
			{
				if((index > 0) && (index-1 != off_hist_index))
				{
					index --;
					if(!off_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(off_str, &off_hist_buff[index]);
						PrintMenu(off_str, &off_str[11]);
					}
				} 
				else if(!index)
				{
					index = 2;
					if(!off_hist_buff[index].year)
					{
						PrintMenu("NO", "DATA");
					}
					else
					{
						TimeToStr(off_str, &off_hist_buff[index]);
						PrintMenu(off_str, &off_str[11]);
					}
				}
				break;
			} //end case RIGTH
			case ESC:
			{
				flag_hist = 0;
				break;
			}	//end case ESC
		} //end switch(button_status)
	} //end while
}




static inline void ClrErrHandler()
{
	extern uint8_t err_cnt;
	err_cnt = 0;
}

static void TimeSetupHandler()	//���������� ��������� �����
{
	uint8_t flag_setup = 1;	//���� ������ � ������� ���������
	RTCtime_t clocks;
	GetRTC(&clocks);	//��������� �������� �������� �������
	
	char time_str[]={"DD-MM-YYYY hh:mm:ss"};
	uint8_t flag_state=0;	//���� ��������� - � ����� �������� ������� ������
	TimeToStr(time_str, &clocks);
	PrintMenu(time_str, &time_str[11]);
	
	while(flag_setup)
	{
		button_status = ButtonSelect();
		switch (button_status)
    {
    	case ENTER:
			{
				switch (flag_state)
				{
					case 0:	// ����
					{
						flag_state=1;
						break;
					}
					case 1:	// �����
					{
						flag_state=2;
						break;
					}
					case 2:	// ���
					{
						flag_state=3;
						break;
					}
					case 3:	// ���
					{
						flag_state=4;
						break;
					}
						case 4:	// ������
					{
						flag_state=5;
						break;
					}
						case 5:	// �������
					{
						flag_state=0;
						break;
					}
					TimeToStr(time_str, &clocks);
					PrintMenu(time_str, &time_str[11]);	
				}
    		break;
			}//end Case ENTER
    	case RIGHT:
			{
				switch (flag_state)
				{
					case 0:	// ����
					{
						if(clocks.date<31)
						{
							clocks.date+=1;
						}
						else if(clocks.date>=31)
						{
							clocks.date=1;
						}
						break;
					}
					case 1:	// �����
					{
						if(clocks.month<12)
						{
							clocks.month+=1;
						}
						else if(clocks.month>=12)
						{
							clocks.month=1;
						}
						break;
					}
					case 2:	// ���
					{
						if(clocks.year<2100)
						{
							clocks.year+=1;
						}
						else if(clocks.year>=2100)
						{
							clocks.year=1;
						}
						break;
					}
					case 3:	// ���
					{
						if(clocks.hour<23)
						{
							clocks.hour+=1;
						}
						else if(clocks.hour>=23)
						{
							clocks.hour=0;
						}
						break;
					}
						case 4:	// ������
					{
						if(clocks.minute<59)
						{
							clocks.minute+=1;
						}
						else if(clocks.minute>=59)
						{
							clocks.minute=0;
						}
						break;
					}
						case 5:	// �������
					{
						if(clocks.second<59)
						{
							clocks.second+=1;
						}
						else if(clocks.second>=59)
						{
							clocks.second=0;
						}
						break;
					}
				}
					TimeToStr(time_str, &clocks);
					PrintMenu(time_str, &time_str[11]);
    		break;
			}//end Case RIGHT
			case LEFT:
			{
				switch (flag_state)
				{
					case 0:	// ����
					{
						if(clocks.date>1)
						{
							clocks.date-=1;
						}
						else if(clocks.date<=1)
						{
							clocks.date=31;
						}
						break;
					}
					case 1:	// �����
					{
						if(clocks.month>1)
						{
							clocks.month-=1;
						}
						else if(clocks.month<=1)
						{
							clocks.month=12;
						}
						break;
					}
					case 2:	// ���
					{
						if(clocks.year>2001)
						{
							clocks.year-=1;
						}
						else if(clocks.year<=2001)
						{
							clocks.year=2100;
						}
						break;
					}
					case 3:	// ���
					{
						if(clocks.hour>0)
						{
							clocks.hour-=1;
						}
						else if(clocks.hour<=0)
						{
							clocks.hour=23;
						}
						break;
					}
						case 4:	// ������
					{
						if(clocks.minute>0)
						{
							clocks.minute-=1;
						}
						else if(clocks.minute<=0)
						{
							clocks.minute=59;
						}
						break;
					}
						case 5:	// �������
					{
						if(clocks.second>0)
						{
							clocks.second-=1;
						}
						else if(clocks.second<=0)
						{
							clocks.second=59;
						}
						break;
					}
				}
					TimeToStr(time_str, &clocks);
					PrintMenu(time_str, &time_str[11]);
    		break;
			}//end case LEFT
			case ESC:
			{
				SetRTC(&clocks);	// �������� ������������� �����
				flag_setup = 0;
    		break;
			}
    	default:
    		break;
    }//end switch
	}//end while
}
