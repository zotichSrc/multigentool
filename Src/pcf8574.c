#include "pcf8574.h"
#include "i2c.h"
void writeToPcf8574(pcf8574_t* pcf8574, uint8_t* pBuf, uint8_t len)
{
	writeDataI2C(pcf8574->pI2C, pcf8574->address, pBuf, len);
}
void readFromPcf8574(pcf8574_t* pcf8574, uint8_t* pBuf, uint8_t len)
{
	readDataI2C(pcf8574->pI2C, pcf8574->address, pBuf, len);
}
