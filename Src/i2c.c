#include "i2c.h"

#define I2C_MODE_WRITE	0x00
#define I2C_MODE_READ		0x01

#define I2C_ADDR(devAddr,mode) ((uint8_t)devAddr<<1 | (uint8_t)mode)

void writeDataI2C(I2C_TypeDef *I2Cx, uint8_t devAddr, uint8_t* data, uint8_t len)
{
	uint16_t count = 0;
	while((I2Cx->SR2 & I2C_SR2_BUSY) && count < 60000)
		count ++;
	
	I2Cx->CR1 |= I2C_CR1_START;
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_SB) && count < 60000)
		count ++;
	(void)I2Cx->SR1;
	
	I2Cx->DR = I2C_ADDR(devAddr,I2C_MODE_WRITE);
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_ADDR) && count < 60000)
		count ++;
	(void)I2Cx->SR1;
	(void)I2Cx->SR2;
	
	for(uint8_t i = 0; i<len; i++)
	{
		count = 0;
		while(!(I2Cx->SR1 & I2C_SR1_TXE) && count < 60000)
			count ++;
		I2Cx->DR = data[i];
	}
	
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_BTF) && count < 60000)
			count ++;
	I2Cx->CR1 |= I2C_CR1_STOP;
}

void readDataI2C(I2C_TypeDef *I2Cx, uint8_t devAddr, uint8_t* data, uint8_t len)
{
	uint16_t count = 0;
	while((I2Cx->SR2 & I2C_SR2_BUSY) && count < 60000)
		count ++;
	I2Cx->CR1 |= I2C_CR1_ACK;
	
	I2Cx->CR1 |= I2C_CR1_START;
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_SB) && count < 60000)
		count ++;
	(void)I2Cx->SR1;
	
	I2Cx->DR = I2C_ADDR(devAddr,I2C_MODE_READ);
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_ADDR) && count < 60000)
		count ++;
	(void)I2Cx->SR1;
	(void)I2Cx->SR2;
	
	for(uint8_t i = 0; i<len-1; i++)
	{
		count = 0;
		while(!(I2Cx->SR1 & I2C_SR1_RXNE) && count < 60000)
			count ++;
		data[i] = I2Cx->DR;
	}
	
	count = 0;
	while(!(I2Cx->SR1 & I2C_SR1_RXNE) && count < 60000)
		count ++;
	data[len-1] = I2Cx->DR;
	I2Cx->CR1 &= ~I2C_CR1_ACK;
	I2Cx->CR1 |= I2C_CR1_STOP;
	
}
