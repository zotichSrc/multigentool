#ifndef __INITRCC_H__
#define __INITRCC_H__

#include "stm32f4xx.h"
#define HSE_STARTUP_TIMEOUT   ((uint16_t)0x0500)

uint8_t InitRCC(void);

#endif /*__INITRCC_H__*/
