#ifndef __I2C_H__
#define __I2C_H__

#include "stm32f4xx.h"
//#include "stdbool.h"



void writeDataI2C(I2C_TypeDef *I2Cx, uint8_t devAddr, uint8_t* data, uint8_t len);
void readDataI2C(I2C_TypeDef *I2Cx, uint8_t devAddr, uint8_t* data, uint8_t len);

#endif /*__I2C_H__*/

