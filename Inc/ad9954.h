#ifndef __AD9954_H__
#define __AD9954_H__

#include "stm32f4xx.h"

#define RESET_CS(REG,PIN) (REG |= PIN << 16)
#define SET_CS(REG,PIN) (REG |= PIN)

#define	AD9954_CFR1		0x00
#define	AD9954_CFR2		0x01
#define	AD9954_ASF		0x02
#define	AD9954_ARR		0x03
#define	AD9954_FTW0		0x04
#define	AD9954_POW0		0x05
#define	AD9954_FTW1		0x06
#define	AD9954_RSCW0	0x07
#define	AD9954_RSCW1	0x08
#define	AD9954_RSCW2	0x09
#define	AD9954_RSCW3	0x0A
#define	AD9954_RAM		0x0B

#define	AD9954_NLSCW	0x07
#define	AD9954_PLSCW	0x08

#define AD9954_READ		0x80
#define AD9954_WRITE	0x00
#define A9954_INSTUCTION_MASK	0x1F

//#define AD9954_CFR1_SYNC_CLK_DISABLE			(uint32_t)(1<<1)
//#define AD9954_CFR1_LIN_SWEEP_NO_DWELL		(uint32_t)(1<<2)
//#define AD9954_CFR1_EXT_PWR_DWN_MODE			(uint32_t)(1<<3)
//#define AD9954_CFR1_CLK_INP_PWR_DWN			(uint32_t)(1<<4)
//#define AD9954_CFR1_DAC_PWR_DWN					(uint32_t)(1<<5)
//#define AD9954_CFR1_COMP_PWR_DWN					(uint32_t)(1<<6)
//#define AD9954_CFR1_DIGITAL_PWR_DWN			(uint32_t)(1<<7)
//#define AD9954_CFR1_LSB_FIRST						(uint32_t)(1<<8)
//#define AD9954_CFR1_SDIO_INPUT_ONLY			(uint32_t)(1<<9)
//#define AD9954_CFR1_CLR_PHASE_ACCUM			(uint32_t)(1<<10)
//#define AD9954_CFR1_CLR_FREQ_ACCUM				(uint32_t)(1<<11)
//#define AD9954_CFR1_SINE_SELECT					(uint32_t)(1<<12)
//#define AD9954_CFR1_AUTO_CLR_PHASE_ACCUM	(uint32_t)(1<<13)
//#define AD9954_CFR1_AUTO_CLR_FRWQ_ACCUM	(uint32_t)(1<<14)
//#define AD9954_CFR1_SRR_LOAD_ENABLE			(uint32_t)(1<<15)
//#define AD9954_CFR1_LINEAR_SWEEP_ENABLE	(uint32_t)(1<<21)
//#define AD9954_CFR1_SW_MAN_SYNC					(uint32_t)(1<<22)
//#define AD9954_CFR1_AUTO_SYNC_ENABLE			(uint32_t)(1<<23)
//#define AD9954_CFR1_AUTO_OSK_ENABLE			(uint32_t)(1<<24)
//#define AD9954_CFR1_OSK_ENABLE						(uint32_t)(1<<25)
//#define AD9954_CFR1_INTER_PROF_CNTRL_0		(uint32_t)(1<<27)
//#define AD9954_CFR1_INTER_PROF_CNTRL_1		(uint32_t)(1<<28)
//#define AD9954_CFR1_INTER_PROF_CNTRL_2		(uint32_t)(1<<29)
//#define AD9954_CFR1_RAM_DESTINATION			(uint32_t)(1<<30)
//#define AD9954_CFR1_RAM_ENABLE						(uint32_t)(1<<31)

typedef enum {SINGLE_TONE = 0, LINEAR_SWEEP, RAM_MODE} AD9954_MODES;
typedef enum {DIRECT_SWITCH = 0, RAMP_UP, BIDIRECTIONAL_RAMP, CONTINUOUS_BIDIR_RAMP, CONTINUOUS_RECIRCULATION} AD9954_RAM_MODES;
typedef enum {PROFILE_0 = 0, PROFILE_1, PROFILE_2, PROFILE_3} AD9954_RAM_PROFILES;
typedef enum {INTPROF_0 = 0, INTPROF_1, INTPROF_2, INTPROF_3, INTPROF_4, INTPROF_5, INTPROF_6} AD9954_RAM_INTERNAL_PROFILES;
typedef enum {TYPE_SINE = 0, TYPE_LINEAR} AD9954_TYPE_SWEEP_TABLE;

#pragma anon_unions

typedef union{
	uint32_t cfr1;
	struct{
		uint32_t unused_1 							:1;
		uint32_t sync_clk_disable 			:1;
		uint32_t linear_sweep_no_dwell	:1;
		uint32_t ext_pwr_dwn_mode				:1;
		uint32_t clk_inp_pwr_dwn				:1;
		uint32_t dac_pwr_dwn						:1;
		uint32_t comp_pwr_dwn						:1;
		uint32_t digital_pwr_dwn				:1;
		uint32_t lsb_first							:1;
		uint32_t sdio_input_only				:1;
		uint32_t clr_phase_accum				:1;
		uint32_t clr_freq_accum					:1;
		uint32_t sine_select						:1;
		uint32_t auto_clr_phase_accum		:1;
		uint32_t auto_clr_freq_accum		:1;
		uint32_t srr_load_enable				:1;
		uint32_t unused_2 							:5;
		uint32_t linear_sweep_enable		:1;
		uint32_t software_manual_sync		:1;
		uint32_t auto_sync_enable				:1;
		uint32_t auto_osk_enable				:1;
		uint32_t osk_enable							:1;
		uint32_t load_arr_ctrl					:1;
		uint32_t inter_profile_ctrl			:3;
		uint32_t ram_destination				:1;
		uint32_t ram_enable							:1;
	};
}CFR1_t;

typedef union{
	uint32_t cfr2;
	struct{
		uint32_t charge_pump_current		:2;
		uint32_t vco_range				 			:1;
		uint32_t refclk_multiplier			:5;
		uint32_t unused_1								:1;
		uint32_t xtal_out_enable				:1;
		uint32_t hardware_manual_sync_en:1;
		uint32_t hight_speed_sync_en		:1;
		uint32_t unused_2								:4;
		uint32_t unused_3								:8;
	};
}CFR2_t;

#pragma pack(push, 1)
typedef struct{
	uint16_t ram_segment_beginning_addr;
	uint16_t ram_segment_final_addr;
	uint16_t ram_segment_addr_ramp_rate;
	struct{
		uint8_t no_dwell_active 			:1;
		AD9954_RAM_MODES ram_segment_mode_ctrl :3;
	};
}RSCW_t;

typedef union{
	uint8_t rscw[5];
	struct{
		uint8_t ram_segment_beginning_addr_H	:4;
		uint8_t no_dwell_active								:1;
		uint8_t ram_segment_mode_ctrl					:3;
		uint8_t ram_segment_final_addr_H			:2;
		uint8_t ram_segment_beginning_addr_L	:6;
		uint8_t ram_segment_final_addr_L			:8;
		uint8_t ram_segment_addr_ramp_rate_H	:8;
		uint8_t ram_segment_addr_ramp_rate_L	:8;
	};
}LL_RSCW_t;

typedef union{
	uint8_t nlscw[5];
	struct{
		uint32_t falling_delta_freq_tun_word	:32;
		uint8_t falling_sweep_ramp_rate				:8;
	};
}NLSCW_t;

typedef union{
	uint8_t plscw[5];
	struct{
		uint32_t rising_delta_freq_tun_word	:32;
		uint8_t rising_sweep_ramp_rate			:8;
	};
}PLSCW_t;

#pragma pack(pop)

#pragma no_anon_unions

typedef struct{
	SPI_TypeDef *pSPI;
	GPIO_TypeDef *CSPort;
	GPIO_TypeDef *IOUPDPort;
	GPIO_TypeDef *OSKPort;
	GPIO_TypeDef *PS0Port;
	GPIO_TypeDef *PS1Port;
	uint16_t CSPin;
	uint16_t IOUPDPin;
	uint16_t OSKPin;
	uint16_t PS0Pin;
	uint16_t PS1Pin;
	uint32_t systemFrequency;
	uint32_t xtalFrequency;
}ad9954_t;


//LL_RSCW_t AD9954_setRscwReg(RSCW_t *rscw);
//RSCW_t AD9954_getRscwReg(LL_RSCW_t *LL_rscw);

void AD9954_generateSweepTable(ad9954_t *ad9954, uint32_t freqCarrier, uint32_t freqSweep, uint32_t *table, uint16_t lenTab, AD9954_TYPE_SWEEP_TABLE sweepType);
void AD9954_setSystemFreq(ad9954_t *ad9954);
void AD9954_setSingleFreq(ad9954_t *ad9954, uint32_t freq);
void AD9954_setPhaseOffset(ad9954_t *ad9954, uint16_t phaseOffset);
void AD9954_setSweepFreq(ad9954_t *ad9954, uint32_t freq_0, uint32_t freq_1, uint32_t posDF, uint8_t posRR, uint32_t negDF, uint8_t negRR);
void AD9954_setOperationMode(ad9954_t *ad9954, AD9954_MODES mode);
//void AD9954_setRamMode(ad9954_t *ad9954, AD9954_RAM_MODES ramMode);
void AD9954_setRamProfileSettings(ad9954_t *ad9954, AD9954_RAM_PROFILES profile, RSCW_t *profSettings);
void AD9954_setRamInternalProfile(ad9954_t *ad9954, AD9954_RAM_INTERNAL_PROFILES profile);
void AD9954_setRamTable(ad9954_t *ad9954, AD9954_RAM_PROFILES profile, uint32_t *table, uint8_t tableLen);
void AD9954_setProfile(ad9954_t *ad9954, AD9954_RAM_PROFILES profile);
/*Low-Level commands*/
void AD9954_writeRegister(ad9954_t *ad9954, uint8_t regAddr, uint8_t *data, uint8_t dataLen);
void AD9954_readRegister(ad9954_t *ad9954, uint8_t regAddr, uint8_t *data, uint8_t dataLen);
//void AD9954_writeInstruction();
//void AD9954_writeData();
void AD9954_updateIO(ad9954_t *ad9954);


#endif /*__AD9954_H__*/
