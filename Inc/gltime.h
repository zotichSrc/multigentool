#ifndef __GLTIME_H__
#define __GLTIME_H__

uint8_t TimeoutStart(void);
void TimeoutStop(void);
uint32_t GetTickTimeout(void);
void Delay_ms(uint32_t time_ms);
void Delay_us(uint32_t time_us);
void SysTick_Handler(void);

#endif /*__GLTIME_H__*/
