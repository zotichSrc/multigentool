#ifndef __PCF8574_H__
#define __PCF8574_H__

#include "stm32f4xx.h"
//#include "stdint.h"


typedef struct
{
	uint8_t address;
	I2C_TypeDef *pI2C;
	GPIO_TypeDef *interruptPort;
	uint16_t intrruptPin;
}pcf8574_t;


void writeToPcf8574(pcf8574_t* pcf8574, uint8_t* pBuf, uint8_t len);
void readFromPcf8574(pcf8574_t* pcf8574, uint8_t* pBuf, uint8_t len);




#endif /*__PCF8574_H__*/
