#ifndef __LCD_H__
#define __LCD_H__

#include "pcf8574.h"

#define LCD_I2C

#define LCD_CLEAR				((uint8_t) 0x01)
#define LCD_HOME				((uint8_t) 0x02)
#define LCD_DDRAM_SET		((uint8_t) 0x80)
#define LCD_NEXT_STR		((uint8_t) 0x40)
#define LCD_CGRAM_SET		((uint8_t) 0x40)

#define LCD_EM					((uint8_t) 0x04)
#define LCD_EM_INC_CNT	((uint8_t) 0x02)
#define LCD_EM_SH_DISP	((uint8_t) 0x01)

#define LCD_SH		((uint8_t) 0x10)
#define LCD_SH_S	((uint8_t) 0x08)
#define LCD_SH_R	((uint8_t) 0x04)

#define LCD_DISP		((uint8_t) 0x08)
#define LCD_DISP_D	((uint8_t) 0x04)
#define LCD_DISP_C	((uint8_t) 0x02)
#define LCD_DISP_B	((uint8_t) 0x01)

void LcdInit(pcf8574_t *pPCF);
void LcdWriteCmd(uint8_t);
void LcdWriteData(uint8_t);
void LcdWriteStr(char*);
void LcdSetCursorPos(uint8_t position);
void LcdClear();
void LcdDisplayOn();
void LcdDisplayOff();
void LcdBacklightOn();
void LcdBacklightOff();




#endif /*__LCD__*/
