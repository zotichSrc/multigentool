#ifndef __SPI_H__
#define __SPI_H__

#include "stm32f4xx.h"

void WriteByteSPI(SPI_TypeDef *SPIx, uint8_t byte);
uint8_t ReadByteSPI(SPI_TypeDef *SPIx);
uint8_t WriteReadByteSPI(SPI_TypeDef *SPIx, uint8_t byte);
void ReadDataSPI(SPI_TypeDef *SPIx, uint8_t *datain, uint32_t len, uint8_t MSBFirst);
void WriteDataSPI(SPI_TypeDef *SPIx, uint8_t* data, uint32_t len, uint8_t MSBFirst);
void WriteReadDataSPI(SPI_TypeDef *SPIx, uint8_t* dataout, uint8_t *datain, uint32_t len, uint8_t MSBFirst);

void WriteReadDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t* dataout, uint8_t *datain, uint8_t len);
void ReadDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t *datain, uint8_t len);
void WriteDataSPI_DMA(SPI_TypeDef *SPIx, uint8_t* data, uint8_t len);


#endif /*__SPI_H__*/
