#include "stm32f4xx.h"
#include "InitRCC.h"
#include "math.h"
#include "i2c.h"
#include "LCD.h"
#include "gltime.h"
#include "gpio.h"
#include "ad9954.h"

#define BAUDRATE(baud)		((84000000 + baud/2)/baud) //������ �������� UART

#define PWM_PERIOD 915
#define PWM_PRESCALER 0

#define SineFreq 100
#define LENGTH_LOOK_UP 4096

#define TIMER_IN_FREQ 60000000
#define TIMER_OUT_FREQ(freq) (freq*PWM_PERIOD)
#define PSC_VALUE(freq) (TIMER_IN_FREQ/TIMER_OUT_FREQ(freq) - 1)

void TIM3_IRQHandler();
void TIM4_IRQHandler();
void InitPeriph(void);


const double Ftimer = 65573.7704918033;
//const double pi = 3.14159265358979;
uint16_t k;
uint16_t M;

int main(void)
{
	InitRCC();
	k = 0;
	M = (uint16_t)round(((double)SineFreq * (double)65535 / (double)Ftimer));
	
//	for(uint32_t i = 0; i<LENGTH_LOOK_UP; i++)
//	{
//		double tmp2 = (double)i/(double)LENGTH_LOOK_UP;
//		double tmp3 = PWM_PERIOD/2 + (PWM_PERIOD/2) * sin(2*pi*tmp2);
//		sine[i] = (uint16_t)tmp3;
//	}
	InitPeriph();
//	NVIC_EnableIRQ(TIM4_IRQn);
//	uint8_t wrData = 0x01;
//	uint8_t rdData = 0x00;
//	pcf8574_t pcf;
//	pcf.address = 0x26;
//	pcf.pI2C = I2C1;
//	LcdInit(&pcf);
//	LcdWriteStr("�������");
//	LcdSetCursorPos(0x40);
//	LcdWriteStr("ABCDEFGHIJKLMNOPQRST");
//	LcdBacklightOff();
//	Delay_ms(20);
//	LcdBacklightOn();
//	Delay_ms(20);
//	LcdDisplayOff();
//	Delay_ms(20);
//	LcdDisplayOn();
	
	ad9954_t ad9954;
	ad9954.CSPin = GPIO_PIN_12;
	ad9954.CSPort = GPIOB;
	ad9954.IOUPDPin = GPIO_PIN_8;
	ad9954.IOUPDPort = GPIOA;
	ad9954.OSKPin = GPIO_PIN_9;
	ad9954.OSKPort = GPIOA;
	ad9954.PS0Pin = GPIO_PIN_10;
	ad9954.PS0Port = GPIOA;
	ad9954.PS1Pin = GPIO_PIN_15;
	ad9954.PS1Port = GPIOA;
	ad9954.pSPI = SPI2;
	ad9954.systemFrequency = 400000000;
	ad9954.xtalFrequency = 20000000;
	
	
	CFR1_t cfr,cfr2;
	cfr.sdio_input_only = 1;
	cfr.auto_clr_freq_accum = 0;
	cfr.auto_clr_phase_accum  = 0;
	cfr.osk_enable = 0;
	cfr.auto_osk_enable = 0;
	cfr.load_arr_ctrl = 0;
	uint32_t freq = 500000;
	
	GPIOB->BSRR |= GPIO_BSRR_BS7;
	Delay_ms(1);
	GPIOB->BSRR |= GPIO_BSRR_BR7;
	Delay_ms(2);
	
	AD9954_writeRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr.cfr1), 4);
	AD9954_updateIO(&ad9954);
	
	AD9954_setSystemFreq(&ad9954);
	
	uint32_t table[200];
	AD9954_generateSweepTable(&ad9954,100000,50000,(uint32_t*)&table,200, TYPE_LINEAR);
	
	RSCW_t rscw0;
	rscw0.ram_segment_beginning_addr = 0;
	rscw0.ram_segment_final_addr = 199;
	rscw0.ram_segment_addr_ramp_rate = 1;
	rscw0.ram_segment_mode_ctrl = CONTINUOUS_RECIRCULATION;
	rscw0.no_dwell_active = 0;
	
	RSCW_t rscw1;
	rscw1.ram_segment_beginning_addr = 200;
	rscw1.ram_segment_final_addr = 299;
	rscw1.ram_segment_addr_ramp_rate = 1;
	rscw1.ram_segment_mode_ctrl = CONTINUOUS_RECIRCULATION;
	rscw1.no_dwell_active = 0;
	
//	uint8_t data[5];
//	uint8_t data2[5] = {8,9,10,11,12};
	
//	AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr2.cfr1),4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr2.cfr1),4);
	
	LL_RSCW_t rscwSettingsLL;
	
	AD9954_setOperationMode(&ad9954, RAM_MODE);
	AD9954_setRamProfileSettings(&ad9954,PROFILE_0,&rscw0);
	AD9954_setRamProfileSettings(&ad9954,PROFILE_1,&rscw1);
	Delay_ms(2);
	AD9954_setRamTable(&ad9954, PROFILE_0, (uint32_t*)&table, 200);
	AD9954_generateSweepTable(&ad9954,100000,50000,(uint32_t*)&table,200, TYPE_SINE);
	AD9954_setRamTable(&ad9954, PROFILE_1, (uint32_t*)&table, 200);
	
	
//	Delay_ms(10000);
//	AD9954_setOperationMode(&ad9954, SINGLE_TONE);
	
//	AD9954_readRegister(&ad9954, AD9954_CFR1, data,4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, data,4);
//	AD9954_readRegister(&ad9954, AD9954_RSCW0, data,5);

//	AD9954_readRegister(&ad9954, AD9954_RSCW0, data,5);
//	AD9954_writeRegister(&ad9954, AD9954_RSCW0, data2,5);
//	AD9954_updateIO(&ad9954);
//	AD9954_readRegister(&ad9954, AD9954_RSCW0, data,5);
//	AD9954_writeRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr.cfr1), 4);
//	AD9954_readRegister(&ad9954, AD9954_RSCW0, data,5);
//	AD9954_readRegister(&ad9954, AD9954_RSCW3, data,5);
//	AD9954_readRegister(&ad9954, AD9954_RSCW0, data,5);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, data,4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, data,4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, data,4);

	

	
	

//		
	
//	
	
	
//	AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr2.cfr1),4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr2.cfr1),4);
//	AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr2.cfr1),4);
	
	
//	LL_RSCW_t tmp1;
//	RSCW_t tmp2;
//	RSCW_t tmp3;
//	tmp2.no_dwell_active = 1;
//	tmp2.ram_segment_addr_ramp_rate = 0xaabb;
//	tmp2.ram_segment_beginning_addr = 0x01dd;
//	tmp2.ram_segment_final_addr = 0x01ff;
//	tmp2.ram_segment_mode_ctrl = 1;
//	tmp1 = AD9954_setRscwReg(&tmp2);
//	tmp3 = AD9954_getRscwReg(&tmp1);
	volatile int q = 1;
	while(1)
	{
		AD9954_setProfile(&ad9954,PROFILE_0);
		Delay_ms(10000);
		AD9954_setProfile(&ad9954,PROFILE_1);
		Delay_ms(10000);
 		
//		GPIOA->BSRR |= GPIO_BSRR_BS10; //ps0
//		Delay_ms(1000);
//		GPIOA->BSRR |= GPIO_BSRR_BR10; //ps0
//		Delay_ms(1000);
//		
		
//			GPIOA->BSRR |= GPIO_BSRR_BR15;	//ps1

		
		
//		GPIOB->BSRR |= GPIO_BSRR_BS15; //MOSI
//		GPIOB->BSRR |= GPIO_BSRR_BR15; //MOSI
//		AD9954_writeRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr.cfr1), 4);
//		AD9954_updateIO(&ad9954);
//		Delay_ms(1);
//		GPIOA->BSRR |= GPIO_BSRR_BS9;
//		Delay_ms(1);
//		GPIOA->BSRR |= GPIO_BSRR_BR9;
//		freq = 10000;
//		for(int i = 0; i<20; i++)
//		{
//			freq += 4000;
//			AD9954_setSingleFreq(&ad9954, freq);
//			Delay_ms(5);
//		}
//		for(int i = 0; i<20; i++)
//		{
//			freq -= 4000;
//			AD9954_setSingleFreq(&ad9954, freq);
//			Delay_ms(5);
//		}
//		AD9954_writeRegister(&ad9954, AD9954_CFR2, (uint8_t*)&(cfr2.cfr2), 3);
//		AD9954_writeRegister(&ad9954, AD9954_CFR1, (uint8_t*)&(cfr.cfr1), 4);
//		AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&data, 4);
//		AD9954_readRegister(&ad9954, AD9954_CFR2, (uint8_t*)&data, 3);
//		AD9954_updateIO(&ad9954);
				

//		AD9954_readRegister(&ad9954, AD9954_CFR1, (uint8_t*)&data, 4);
//		Delay_ms(10);
//		AD9954_writeRegister(&ad9954, AD9954_CFR2, (uint8_t*)&(cfr2.cfr2), 3);
//		AD9954_updateIO(&ad9954);
//		Delay_ms(10);
//		AD9954_readRegister(&ad9954, AD9954_CFR2, (uint8_t*)&data, 3);
//		Delay_ms(10);
//		GPIOB->MODER &= ~GPIO_MODER_MODE15;
//		GPIOB->MODER |= GPIO_MODER_MODE15_0;
//		GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD15;
//		Delay_ms(5);
//		GPIOB->BSRR |= GPIO_BSRR_BR15; //MOSI
//		GPIOB->MODER &= ~GPIO_MODER_MODE15;
//		Delay_ms(10);
//		AD9954_readRegister(&ad9954, AD9954_ASF, (uint8_t*)&data, 2);
//		Delay_ms(10);
//		GPIOB->BSRR |= GPIO_BSRR_BR12;
//		Delay_ms(10);
//		GPIOB->BSRR |= GPIO_BSRR_BS12;
		
	}
}

void TIM3_IRQHandler()
{
	TIM3->SR &= ~TIM_SR_UIF;

	uint16_t tmp = k >> 4;
//	if(pila[tmp] == 0)
//		TIM3->CCR1 = 1;
//	else
//		TIM3->CCR1 = pila[tmp];
	k+=M;
}

void InitPeriph(void)
{
	/*������� 60 ���*/
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN;
//	GPIOA->MODER |= GPIO_MODER_MODE8_1;
//	GPIOA->OTYPER &= ~GPIO_OTYPER_OT8;
//	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8_0 | GPIO_OSPEEDER_OSPEEDR8_1;
//	GPIOA->PUPDR &= ~ GPIO_PUPDR_PUPD8;
//	GPIOA->AFR[1] &= ~GPIO_AFRH_AFSEL8;
//	RCC->CFGR |= RCC_CFGR_MCO1; 
//	RCC->CFGR &= ~RCC_CFGR_MCO1PRE_Pos;
	
	/*��� ���������*/	
	RCC->AHB1ENR |= RCC_AHB1ENR_GPIOBEN;
	GPIOB->MODER |= GPIO_MODER_MODE4_1;
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT4;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR4_0 | GPIO_OSPEEDER_OSPEEDR4_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD4;
	GPIOB->AFR[0] |= GPIO_AFRL_AFSEL4_1;
	
	RCC->APB1ENR |= RCC_APB1ENR_TIM3EN;
	
	TIM3->ARR = PWM_PERIOD;
	TIM3->PSC = PWM_PRESCALER;
	TIM3->CCR1 = PWM_PERIOD/2;
	TIM3->CCER |= TIM_CCER_CC1E;
	TIM3->CCMR1 |= TIM_CCMR1_OC1M_1 | TIM_CCMR1_OC1M_2;
	TIM3->DIER |= TIM_DIER_UIE;

	TIM3->CR1 |= TIM_CR1_CEN;
	
	/*I2C-�������*/
	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN;
	I2C1->CR1 &= ~I2C_CR1_SMBUS;
	I2C1->CR2 &= ~I2C_CR2_FREQ;
	I2C1->CR2 |= 30;
	I2C1->CCR &= ~(I2C_CCR_FS|I2C_CCR_DUTY);
	I2C1->CCR |= 150; //(1/100000)/(2*(1/(30*10^6)))
	I2C1->TRISE = 31; //((1000*10^-9) / (1/(30*10^6)) + 1
	I2C1->CR1 |= I2C_CR1_PE;
	
	GPIOB->MODER |= GPIO_MODER_MODE8_1;
	GPIOB->OTYPER |= GPIO_OTYPER_OT8;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8_0 | GPIO_OSPEEDER_OSPEEDR8_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD8;
	GPIOB->AFR[1] |= GPIO_AFRH_AFSEL8_2;
	
	GPIOB->MODER |= GPIO_MODER_MODE9_1;
	GPIOB->OTYPER |= GPIO_OTYPER_OT9;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9_0 | GPIO_OSPEEDER_OSPEEDR9_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD9;
	GPIOB->AFR[1] |= GPIO_AFRH_AFSEL9_2;
	
	/*SPI-AD9954-���*/
	//SCK
	GPIOB->MODER |= GPIO_MODER_MODE13_1;
	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT13;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13_0 | GPIO_OSPEEDER_OSPEEDR13_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD13;
	GPIOB->AFR[1] |= GPIO_AFRH_AFSEL13_0 | GPIO_AFRH_AFSEL13_2;
	
//	GPIOB->MODER &= ~GPIO_MODER_MODE13;
//	GPIOB->MODER |= GPIO_MODER_MODE13_0;
//	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT13;
//	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR13_0 | GPIO_OSPEEDER_OSPEEDR13_1;
//	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD13;
//	GPIOB->BSRR |= GPIO_BSRR_BR13;
	
	//MISO
	GPIOB->MODER |= GPIO_MODER_MODE14_1;
	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT14;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR14_0 | GPIO_OSPEEDER_OSPEEDR14_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD14;
	GPIOB->AFR[1] |= GPIO_AFRH_AFSEL14_0 | GPIO_AFRH_AFSEL14_2;

	//MOSI
	GPIOB->MODER |= GPIO_MODER_MODE15_1;
	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT15;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_0 | GPIO_OSPEEDER_OSPEEDR15_1;
	//GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD15;
	GPIOB->PUPDR |= GPIO_PUPDR_PUPD15_1;
	GPIOB->AFR[1] |= GPIO_AFRH_AFSEL15_0 | GPIO_AFRH_AFSEL15_2;

//	GPIOB->MODER &= ~GPIO_MODER_MODE15;
//	GPIOB->MODER |= GPIO_MODER_MODE15_0;
//	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT15;
//	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_0 | GPIO_OSPEEDER_OSPEEDR15_1;
//	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD15;
//	GPIOB->BSRR |= GPIO_BSRR_BR15;

	//CS-AD9954
	GPIOB->MODER |= GPIO_MODER_MODE12_0;
	GPIOB->OTYPER &= ~ GPIO_OTYPER_OT12;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR12_0 | GPIO_OSPEEDER_OSPEEDR12_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD12;
	GPIOB->BSRR |= GPIO_BSRR_BS12;
	
	
	RCC->APB1ENR |= RCC_APB1ENR_SPI2EN;
	SPI2->CR1 |= SPI_CR1_MSTR;
	SPI2->CR1 |= SPI_CR1_SSM;
	SPI2->CR1 |= SPI_CR1_SSI;
	SPI2->CR1 &= ~SPI_CR1_BR;
//	SPI2->CR1 |= SPI_CR1_CPHA;
//	SPI2->CR1 |= SPI_CR1_CPOL;
	SPI2->CR1 |= SPI_CR1_BR_0|SPI_CR1_BR_1|SPI_CR1_BR_2;
	SPI2->CR1 |= SPI_CR1_SPE;
	
	//SYNCCLK
	//IOSYNC
	
	//IOUPD
	GPIOA->MODER |= GPIO_MODER_MODE8_0;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT8;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR8_0 | GPIO_OSPEEDER_OSPEEDR8_1;
	GPIOA->PUPDR &= ~ GPIO_PUPDR_PUPD8;
	GPIOA->BSRR |= GPIO_BSRR_BR8;
	//OSK
	GPIOA->MODER |= GPIO_MODER_MODE9_0;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT9;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR9_0 | GPIO_OSPEEDER_OSPEEDR9_1;
	GPIOA->PUPDR &= ~ GPIO_PUPDR_PUPD9;
	GPIOA->BSRR |= GPIO_BSRR_BR9;
	//PS0
	GPIOA->MODER |= GPIO_MODER_MODE10_0;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT10;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR10_0 | GPIO_OSPEEDER_OSPEEDR10_1;
	GPIOA->PUPDR &= ~ GPIO_PUPDR_PUPD10;
	GPIOA->BSRR |= GPIO_BSRR_BS10;
	//PS1
	GPIOA->MODER &= ~GPIO_MODER_MODE15;
	GPIOA->MODER |= GPIO_MODER_MODE15_0;
	GPIOA->OTYPER &= ~GPIO_OTYPER_OT15;
	GPIOA->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR15_0 | GPIO_OSPEEDER_OSPEEDR15_1;
	GPIOA->PUPDR &= ~ GPIO_PUPDR_PUPD15;
	GPIOA->BSRR |= GPIO_BSRR_BS15;
	//RESET
	GPIOB->MODER |= GPIO_MODER_MODE7_0;
	GPIOB->OTYPER &= ~GPIO_OTYPER_OT7;
	GPIOB->OSPEEDR |= GPIO_OSPEEDER_OSPEEDR7_0 | GPIO_OSPEEDER_OSPEEDR7_1;
	GPIOB->PUPDR &= ~ GPIO_PUPDR_PUPD7;
	GPIOB->BSRR |= GPIO_BSRR_BS7;
	//
	
	

	/*I2C-�����������*/
	/*Timer-�������*/
	/**/
	/*UART-��*/
	USART6->BRR = BAUDRATE(115200);
	USART6->CR1 |= USART_CR1_UE|USART_CR1_RXNEIE|USART_CR1_TE|USART_CR1_RE;
	/*UART-����*/
	USART2->BRR = BAUDRATE(115200);
	USART2->CR1 |= USART_CR1_UE|USART_CR1_RXNEIE|USART_CR1_TE|USART_CR1_RE;
	
	/*������ ��������� JTAG*/
	
}